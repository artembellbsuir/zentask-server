import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthController } from '../auth/auth.controller';
import { PrismaService } from '../services/prisma.service';
import { UserService } from '../users/users.service';
import { ConfigModule } from '@nestjs/config';
import { BoardsController } from '../boards/boards.controller';
import { BoardsService } from '../boards/boards.service';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { AuthService } from '../auth/auth.service';
import { BoardsModule } from '../boards/boards.module';
import { ListsModule } from '../lists/lists.module';
import { CardsModule } from '../cards/cards.module';
import { ListsService } from '../lists/lists.service';
import { CardsService } from '../cards/cards.service';
import { CredentialsModule } from '../credentials/credentials.module';
import { CredentialsController } from '../credentials/credentials.controller';
@Module({
  imports: [
    ConfigModule.forRoot(),
    AuthModule,
    UsersModule,
    BoardsModule,
    ListsModule,
    CardsModule,
    CredentialsModule,
  ],
  controllers: [
    AppController,
    AuthController,
    BoardsController,
    CredentialsController,
  ],
  providers: [
    AppService,
    UserService,
    PrismaService,
    AuthService,
    BoardsService,
    ListsService,
    CardsService,
  ],
})
export class AppModule {}
