import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { UserService } from '../users/users.service';
import { RegisterData, UserIniqueFields } from 'src/types/RegisterData';
import { sign } from 'jsonwebtoken';
import { compareSync } from 'bcrypt';
import { verify } from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';
import { AuthGuard } from '@nestjs/passport';
import { Board, Card, List } from '.prisma/client';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ListsService } from './lists.service';

@Controller('/api/v1/lists')
export class ListsController {
  constructor(
    private readonly userService: UserService,
    private readonly listsService: ListsService,
  ) {}

  @Get(':listId')
  async getList(
    @Request() request,
    @Param('listId') listId: string,
  ): Promise<List> {
    const { user }: any = request;
    return this.listsService.getListById(parseInt(listId));
  }

  @Put(':listId')
  async updateList(
    @Request() request,
    @Body() body: { title: string },
    @Param('listId') listId: string,
  ): Promise<List> {
    const { user }: any = request;
    return this.listsService.updateList(parseInt(listId), body.title);
  }

  @Delete(':listId')
  async deleteBoard(
    @Req() request: Request,
    @Param('listId') listId: string,
  ): Promise<List> {
    const { user }: any = request;
    return this.listsService.deleteList(parseInt(listId));
  }

  @Get(':listId/cards')
  async getBoardLists(
    @Req() request: Request,
    @Param('listId') listId: string,
  ): Promise<Card[]> {
    const { user }: any = request;
    return this.listsService.getListCards(parseInt(listId));
  }

  @Post(':listId/cards')
  async createListCard(
    @Req() request: Request,
    @Param('listId') listId: string,
    @Body() body: { title: string },
  ): Promise<List> {
    const { user }: any = request;
    return this.listsService.createListCard(parseInt(listId), body.title);
  }
}
