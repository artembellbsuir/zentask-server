import { UseGuards } from '@nestjs/common';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { SocketAuthGuard } from '../auth/socket-auth.guard';
import { ListsService } from './lists.service';

@WebSocketGateway()
export class ListsGateway {
  constructor(private readonly listService: ListsService) {}

  @UseGuards(SocketAuthGuard)
  @SubscribeMessage('post/lists/{{listId}}/cards')
  async createCard(@MessageBody() data: any): Promise<any> {
    return this.listService.createListCard(
      parseInt(data.listId),
      data.title,
    );
  }

  @UseGuards(SocketAuthGuard)
  @SubscribeMessage('delete/lists/{{listId}}')
  async deleteList(@MessageBody() data: any): Promise<any> {
    return this.listService.deleteList(parseInt(data.listId));
  }
}
