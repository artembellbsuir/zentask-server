import { Card, List } from '.prisma/client';
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../services/prisma.service';

@Injectable()
export class ListsService {
  constructor(private prisma: PrismaService) {}

  async updateList(listId: number, title: string): Promise<List> {
    return this.prisma.list.update({
      where: {
        id: listId,
      },
      data: {
        title,
      },
    });
  }

  async getListById(listId: number): Promise<List> {
    return this.prisma.list.findUnique({
      where: {
        id: listId,
      },
      include: {
        cards: true,
      },
    });
  }

  async deleteList(listId: number): Promise<List> {
    return this.prisma.list.delete({
      where: {
        id: listId,
      },
    });
  }

  async getListCards(listId: number): Promise<Card[]> {
    return this.prisma.card.findMany({
      where: {
        list_id: listId,
      },
    });
  }

  async createListCard(listId: number, title: string): Promise<List> {
    return this.prisma.list.update({
      where: {
        id: listId,
      },
      data: {
        cards: {
          create: {
            title,
            text: '',
          },
        },
      },
      include: {
        cards: true,
      },
    });
  }
}
