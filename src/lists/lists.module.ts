import { Module } from '@nestjs/common';
import { ListsService } from './lists.service';
import { PrismaService } from '../services/prisma.service';
import { ListsController } from './lists.controller';
import { UsersModule } from '../users/users.module';
import { UserService } from '../users/users.service';
import { ListsGateway } from './lists.gateway';

@Module({
  imports: [UsersModule],
  providers: [ListsService, PrismaService, ListsGateway],
  controllers: [ListsController],
})
export class ListsModule {}
