import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import { AuthService } from '../auth/auth.service';
import { LocalAuthGuard } from '../auth/local-auth.guard';
import { RegisterData, UserIniqueFields } from '../types/RegisterData';
import { UserService } from '../users/users.service';

@Controller('/api/v1/credentials')
export class CredentialsController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

  @Post('register')
  async register(@Body() registerData: RegisterData): Promise<string> {
    const { username, email }: UserIniqueFields = registerData;

    if (await this.userService.existsUser({ username, email })) {
      return 'User with such username or email already exists.';
    }

    if (await this.userService.createUser(registerData)) {
      return 'You were successfully registered.';
    }
    return 'Some error happened during registration process.';
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }
}
