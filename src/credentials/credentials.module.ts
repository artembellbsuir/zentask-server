import { Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { CredentialsController } from './credentials.controller';

@Module({
  imports: [UsersModule, AuthModule],
  controllers: [CredentialsController],
})
export class CredentialsModule {}
