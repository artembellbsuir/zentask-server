export interface RegisterData {
  firstName: string;
  lastName: string;
  email: string;
  username: string;
  password: string;
}

export interface LoginData {
  login: string;
  password: string;
}

export interface UserIniqueFields {
  username: string;
  email: string;
}
