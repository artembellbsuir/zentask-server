import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { verify } from 'jsonwebtoken';
import { UserService } from '../users/users.service';

@Injectable()
export class SocketAuthGuard implements CanActivate {
  constructor(private readonly userService: UserService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // console.log(context);
    const client = context.switchToWs().getClient();
    const data = context.switchToWs().getData();
    console.log(data);
    const { accessToken } = data;

    const decoded: any = verify(accessToken, process.env.JWT_SECRET);

    context.switchToWs().getData().user = this.userService.getUser(
      decoded.username,
    );
    // console.log();
    return Boolean(this.userService.existsUserByUsername(decoded.username));
    // return validateRequest(request);
    // return true;
  }
}
