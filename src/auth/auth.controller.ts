import { Body, Controller, Post, Request, Res, UseGuards } from '@nestjs/common';
import { UserService } from '../users/users.service';
import { RegisterData, UserIniqueFields } from 'src/types/RegisterData';
import { LocalAuthGuard } from './local-auth.guard';
import { AuthService } from './auth.service';

@Controller('/api/v1/auth')
export class AuthController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

  @Post('register')
  async register(@Body() registerData: RegisterData): Promise<string> {
    const { username, email }: UserIniqueFields = registerData;

    if (await this.userService.existsUser({ username, email })) {
      return 'User with such username or email already exists.';
    }

    if (await this.userService.createUser(registerData)) {
      return 'You were successfully registered.';
    }
    return 'Some error happened during registration process.';
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    console.log('lp');
    
    return this.authService.login(req.user);
  }

  // @Post('login')
  // async login(
  //   @Body() loginData: { username: string; password: string },
  //   @Res() response: Response,
  // ): Promise<any> {
  //   const { username, password } = loginData;

  //   console.log('--/');
  //   const user = await this.userService.getUser(username);

  //   if (!user || !compareSync(password, user.password)) {
  //     return response.status(403).end('Incorrect credentials.');
  //   }

  //   const refreshToken = uuidv4();

  //   const secret = process.env.JWT_SECRET;
  //   console.log(secret);
  //   const accessToken = sign({ id: user.id }, secret);

  //   response.cookie('accessToken', accessToken, {
  //     maxAge: 60 * 60 * 24 * 7 * 1000,
  //     httpOnly: true,
  //     path: '/',
  //   });

  //   return response.status(200).send('You were successfully logged in.');
  // }
}
