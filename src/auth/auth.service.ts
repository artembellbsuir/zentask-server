import { Injectable } from '@nestjs/common';
import { UserService } from '../users/users.service';
import { compareSync } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    console.log('validate');

    console.log(username, pass);
    const user = await this.userService.getUser(username);
    console.log(user);

    if (user && compareSync(pass, user.password)) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.username, userId: user.id };
    console.log('payload when login: ', payload);
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
