import { Injectable } from '@nestjs/common';
import { PrismaService } from '../services/prisma.service';
import { User, Prisma } from '@prisma/client';
import { RegisterData, UserIniqueFields } from '../types/RegisterData';
import { hashSync } from 'bcrypt';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async createUser(data: RegisterData): Promise<User> {
    const { firstName, lastName, email, username, password } = data;

    console.log(data);

    return this.prisma.user.create({
      data: {
        firstName,
        lastName,
        email,
        username,
        password: hashSync(password, 3),
      },
    });
  }

  async existsUserByUsername(username: string) {
    // const { username, email } = data;

    return this.prisma.user.findFirst({
      where: {
        username: {
          equals: username,
        },
      },
    });
  }

  async existsUser(data: UserIniqueFields) {
    const { username, email } = data;

    return this.prisma.user.findFirst({
      where: {
        OR: [
          {
            username: {
              equals: username,
            },
          },
          {
            email: {
              equals: email,
            },
          },
        ],
      },
    });
  }

  async getUser(username: string) {
    return this.prisma.user.findUnique({
      where: {
        username,
      },
    });
  }
}
