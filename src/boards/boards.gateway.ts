import { UseGuards } from '@nestjs/common';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { SocketAuthGuard } from '../auth/socket-auth.guard';
import { BoardsService } from './boards.service';

@WebSocketGateway()
export class BoardsGateway {
  constructor(private readonly boardService: BoardsService) {}

  @UseGuards(SocketAuthGuard)
  @SubscribeMessage('get/boards')
  async getBoards(@MessageBody() data: any): Promise<Array<object>> {
    return this.boardService.getBoardShorts(data.user.userId);
  }

  @UseGuards(SocketAuthGuard)
  @SubscribeMessage('get/boards/{{boardId}}')
  async getBoardById(@MessageBody() data: any): Promise<any> {
    return this.boardService.getBoard(parseInt(data.boardId));
  }

  @UseGuards(SocketAuthGuard)
  @SubscribeMessage('post/boards/{{boardId}}/lists')
  async createList(@MessageBody() data: any): Promise<any> {
    return this.boardService.createBoardList(data.boardId, data.title);
  }

  @UseGuards(SocketAuthGuard)
  @SubscribeMessage('post/boards')
  async createBoard(@MessageBody() data: any): Promise<any> {
    return this.boardService.createBoard(data.title, data.user.userId);
  }

  @UseGuards(SocketAuthGuard)
  @SubscribeMessage('delete/boards/{{boardId}}')
  async deleteBoard(@MessageBody() data: any): Promise<any> {
    return this.boardService.deleteBoard(parseInt(data.boardId));
  }
}
