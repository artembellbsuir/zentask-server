import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { UserService } from '../users/users.service';
import { BoardsService } from './boards.service';
import { RegisterData, UserIniqueFields } from 'src/types/RegisterData';
import { sign } from 'jsonwebtoken';
import { compareSync } from 'bcrypt';
import { verify } from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';
import { AuthGuard } from '@nestjs/passport';
import { Board, List } from '.prisma/client';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller('/api/v1/boards')
export class BoardsController {
  constructor(
    private readonly userService: UserService,
    private readonly boardService: BoardsService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get('/')
  async boards(@Request() request): Promise<Board[]> {
    const user = request.user;
    const boards = await this.boardService.getBoardShorts(user.userId);

    console.log(user);

    return boards;
  }

  @UseGuards(JwtAuthGuard)
  @Post('/')
  async createBoard(
    @Body() body: { title: string },
    @Request() request,
  ): Promise<Board> {
    const { user }: any = request;
    return this.boardService.createBoard(body.title, user.userId);
  }

  @Get(':boardId')
  async getBoard(
    @Request() request,
    @Param('boardId') boardId: string,
  ): Promise<Board> {
    const { user }: any = request;
    return this.boardService.getBoard(parseInt(boardId));
  }

  @Put(':boardId')
  async updateBoard(
    @Request() request,
    @Body() body: { title: string },
    @Param('boardId') boardId: string,
  ): Promise<Board> {
    const { user }: any = request;
    return this.boardService.updateBoard(parseInt(boardId), body.title);
  }

  @Delete(':boardId')
  async deleteBoard(
    @Req() request: Request,
    @Param('boardId') boardId: string,
  ): Promise<Board> {
    const { user }: any = request;
    return this.boardService.deleteBoard(parseInt(boardId));
  }

  @Get(':boardId/lists')
  async getBoardLists(
    @Req() request: Request,
    @Param('boardId') boardId: string,
  ): Promise<List[]> {
    const { user }: any = request;
    return this.boardService.getBoardLists(parseInt(boardId));
  }

  @Post(':boardId/lists')
  async createBoardList(
    @Req() request: Request,
    @Param('boardId') boardId: string,
    @Body() body: { title: string },
  ): Promise<Board> {
    const { user }: any = request;
    return this.boardService.createBoardList(parseInt(boardId), body.title);
  }
}
