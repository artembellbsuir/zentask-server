import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { PrismaService } from '../services/prisma.service';
import { BoardsGateway } from './boards.gateway';
import { BoardsService } from './boards.service';
import { BoardsController } from './boards.controller'

@Module({
  imports: [UsersModule],
  providers: [BoardsService, PrismaService, BoardsGateway],
  controllers: [BoardsController]
})
export class BoardsModule {}
