import { Board, List } from '.prisma/client';
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../services/prisma.service';

@Injectable()
export class BoardsService {
  constructor(private prisma: PrismaService) {}

  async getBoardShorts(id: number): Promise<Board[]> {
    return this.prisma.board.findMany({
      where: {
        user_id: id,
      },
    });
  }

  async getBoardLists(boardId: number): Promise<List[]> {
    return this.prisma.list.findMany({
      where: {
        board_id: boardId,
      },
      include: {
        cards: true,
      },
    });
  }

  async createBoardList(boardId: number, title): Promise<Board> {
    return this.prisma.board.update({
      where: {
        id: boardId,
      },
      data: {
        lists: {
          create: {
            title,
          },
        },
      },
      include: {
        lists: {
          include: {
            cards: true,
          },
        },
      },
    });
  }

  async createBoard(title: string, userId: number): Promise<Board> {
    return this.prisma.board.create({
      data: {
        title,
        user_id: userId,
        lists: {
          create: {
            title: 'This is your first list!',
            cards: {
              create: {
                title: 'And this is your first card',
              },
            },
          },
        },
      },
      include: {
        lists: {
          include: {
            cards: true,
          },
        },
      },
    });
  }

  async getBoard(boardId: number) {
    return await this.prisma.board.findUnique({
      where: {
        id: boardId,
      },
      include: {
        lists: {
          include: {
            cards: true,
          },
        },
      },
    });
  }

  async updateBoard(boardId: number, title: string) {
    return await this.prisma.board.update({
      where: {
        id: boardId,
      },
      data: {
        title,
      },
      include: {
        lists: {
          include: {
            cards: true,
          },
        },
      },
    });
  }

  async deleteBoard(boardId: number) {
    return await this.prisma.board.delete({
      where: {
        id: boardId,
      },
    });
  }
}
