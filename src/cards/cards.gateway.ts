import { UseGuards } from '@nestjs/common';
import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { SocketAuthGuard } from '../auth/socket-auth.guard';
import { CardsService } from './cards.service';

@WebSocketGateway()
export class CardsGateway {
  constructor(private readonly cardsService: CardsService) {}

  @UseGuards(SocketAuthGuard)
  @SubscribeMessage('delete/cards/{{cardId}}')
  async deleteCard(@MessageBody() data: any): Promise<any> {
    return this.cardsService.deleteCard(parseInt(data.cardId));
  }

  @UseGuards(SocketAuthGuard)
  @SubscribeMessage('put/cards/{{cardId}}')
  async deleteList(@MessageBody() data: any): Promise<any> {
    return this.cardsService.updateCard(
      parseInt(data.cardId),
      data.title,
      data.text,
    );
  }
}
