import { Module } from '@nestjs/common';
import { CardsService } from './cards.service';
import { PrismaService } from '../services/prisma.service';
import { CardsController } from './cards.controller';
import { CardsGateway } from './cards.gateway';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [UsersModule],
  providers: [CardsService, PrismaService, CardsGateway],
  controllers: [CardsController],
})
export class CardsModule {}
