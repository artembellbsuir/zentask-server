import { Card } from '.prisma/client';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Request,
  RequestMapping,
} from '@nestjs/common';
import { CardsService } from './cards.service';

@Controller('/api/v1/cards')
export class CardsController {
  constructor(private readonly cardsService: CardsService) {}

  @Get(':cardId')
  async getCard(
    @Request() request,
    @Param('cardId') cardId: string,
  ): Promise<Card> {
    const { user }: any = request;
    return this.cardsService.getCard(parseInt(cardId));
  }

  @Put(':cardId')
  async putCard(
    @Request() request,
    @Body() body: { title: string, text: string },
    @Param('cardId') cardId: string,
  ): Promise<Card> {
    const { user }: any = request;
    return this.cardsService.updateCard(parseInt(cardId), body.title, body.text);
  }

  @Delete(':cardId')
  async deleteCard(
    @Request() request,
    @Param('cardId') cardId: string,
  ): Promise<Card> {
    const { user }: any = request;
    return this.cardsService.deleteCard(parseInt(cardId));
  }
}
