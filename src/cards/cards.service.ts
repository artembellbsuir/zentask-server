import { Card, List } from '.prisma/client';
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../services/prisma.service';

@Injectable()
export class CardsService {
  constructor(private prisma: PrismaService) {}

  async getCard(cardId: number): Promise<Card> {
    return this.prisma.card.findUnique({
      where: {
        id: cardId,
      },
    });
  }

  async deleteCard(cardId: number): Promise<any> {
    return this.prisma.card.delete({
      where: {
        id: cardId,
      },
    });
  }

  async updateCard(cardId: number, title: string, text: string): Promise<any> {
    return this.prisma.card.update({
      where: {
        id: cardId,
      },
      data: {
        title,
        text,
      },
    });
  }
}
